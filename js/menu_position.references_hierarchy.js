(function ($) {

/**
 * Provide the summary information for the references_hierarchy plugin's vertical tab.
 */
Drupal.behaviors.menuPositionReferencesHierarchySettingsSummary = {
  attach: function (context) {
    $('fieldset#edit-references-hierarchy', context).drupalSetSummary(function (context) {
      var vals = [];
      $('input[type="checkbox"]:checked', context).each(function () {
        vals.push($.trim($(this).next('label').find('strong').text()));
      });
      if (!vals.length) {
        vals.push(Drupal.t('Disabled'));
      }
      return vals.join(', ');
      /*if (vals.length > 0) {
        return vals.length + ' parent fields selected';
      }
      else {
        return 'Disabled';
      }*/
    });
  }
};

})(jQuery);
