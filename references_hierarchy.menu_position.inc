<?php
/**
 * @file
 * Provides the references_hierarchy rule plugin for the Menu Position module.
 */

/**
 * Checks if the current node has ancestors.
 * 
 * If yes, then... 
 * a) reset the parent menu link to the menu link of the closest parent node.
 * b) store list of parent nodes (ancestors) for later use.
 * c) return TRUE.
 *
 * @param $variables
 *   An array containing each of the variables saved in the database necessary
 *   to evaluate this condition of the rule.
 * @return
 *   TRUE if condition applies successfully. Otherwise FALSE.
 */
function references_hierarchy_menu_position_condition(&$variables) {
  // exit if we're not in a node context
  if (!isset($variables['context']['node'])) return FALSE;
  
  $ancestors = references_hierarchy_load_ancestors($variables['context']['node'], $variables['parent_fields'], $variables['rule']->menu_name);
  
  // no ancestors? nothing to do
  if (empty($ancestors)) return FALSE;
  
  // set active menu item
  foreach ($ancestors as $parent) {
    // use first available menu link
    if (!empty($parent['mlid'])) {
      $variables['rule']->mlid = $parent['mlid'];
      $variables['rule']->plid = $parent['plid'];
      menu_position_expand_parent_link($parent['plid']);
      break;
    }
  }

  // store ancestors for creation of dynamic breadcrumbs later
  references_hierarchy_current_ancestors($ancestors);
  
  // indicate that this was a success! -> causes mlid/plid changes above to be applied to menu tree
  return TRUE;
}

/**
 * Adds form elements for the references_hierarchy plugin to the rule configuration form.
 *
 * If this condition was active in the current rule, the plug-in variables will
 * be available in $form_state['#menu-position-rule']['conditions']['references_hierarchy'].
 *
 * It is the resposibility of this hook to add any necessary form validation
 * and submission handlers.
 *
 * @param $form
 *   A reference to the "add/edit rule" form array. New form elements should
 *   be added directly to this array.
 * @param $form_state
 *   A reference to the current form state.
 */
function references_hierarchy_menu_position_rule_form(&$form, &$form_state) {  
  // If this is an existing rule, load the variables stored in the rule for this plugin.
  $variables = !empty($form_state['#menu-position-rule']['conditions']['references_hierarchy']) ? $form_state['#menu-position-rule']['conditions']['references_hierarchy'] : array();

  // To ensure that the plugin's form elements are placed inside vertical tabs,
  // all elements should be placed inside a collapsed fielset inside the
  // $form['conditions'] array.
  $form['conditions']['references_hierarchy'] = array(
    '#type' => 'fieldset',
    '#title' => t('References Hierarchy'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attached' => array(
      // Ensures a proper summary is added to its vertical tab.
      'js' => array(drupal_get_path('module', 'references_hierarchy') . '/js/menu_position.references_hierarchy.js'),
    ),
  );
  
  $fields = field_info_fields();
  $options = array();
  foreach ($fields as $key => $field) {
    if ($field['type'] == 'node_reference' && !empty($field['bundles'])) {
      $title = '<strong>' . $field['field_name'] . '</strong>';
      $usage = '';
      foreach ($field['bundles'] as $bundle => $types) {
        if (empty($types)) continue;
        $usage .= empty($usage) ? ' (' : ', ';
        $usage .= $bundle . ':' . implode(', ' . $bundle . ':', $types);
      }
      $title .= empty($usage) ? '' : $usage . ')';
      $options[$key] = $title;
    }
  }
    
  $form['conditions']['references_hierarchy']['parent_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('References for parent nodes'),
    '#default_value' => !empty($variables['parent_fields']) ? $variables['parent_fields'] : array(),
    '#description' => t('Select all node reference fields which are used for storing <em>parent nodes</em>.'),
    '#options' => $options,
  );

  // If we have a validation handler, we can add it this way. Or we could add
  // a per-element validation handler with '#element_validate' above.
  //$form['#validate'][] = 'references_hierarchy_menu_position_rule_form_validate';

  // Add a submit handler.
  $form['#submit'][] = 'references_hierarchy_menu_position_rule_form_submit';
}

/**
 * Validates the plugin's rule form elements.
 *
 * @param $form
 *   A reference to the "add/edit rule" form array.
 * @param $form_state
 *   A reference to the current form state, including submitted values.
 * @todo remove? not being used at the moment
 */
function references_hierarchy_menu_position_rule_form_validate(&$form, &$form_state) {
  if (!empty($form_state['values']['parent_fields'])) {
    form_error($form['conditions']['references_hierarchy']['parent_fields'], t('No soup for you!'));
  }
}

/**
 * Prepares the plugin's variables to be stored in the rule.
 *
 * If the plugin's form elements indicate that the condition needs to be
 * included with the rule, the submit handler must, at the very least, set:
 * $form_state['conditions']['references_hierarchy'] = array(). Optionally, the
 * plugin can add to this array any static variables to be stored in the
 * database with the rule configuration.
 *
 * If, after this submit handler is run, the
 * $form_state['conditions']['references_hierarchy'] variables array is not
 * set, this plugin will not be added as a condition for this rule.
 *
 * @param $form
 *   A reference to the "add/edit rule" form array.
 * @param $form_state
 *   A reference to the current form state, including submitted values.
 */
function references_hierarchy_menu_position_rule_form_submit(&$form, &$form_state) {
  if (!empty($form_state['values']['parent_fields'])) {
    if ($form_state['values']['parent_fields']) {
      $variables = array(
        'parent_fields' => array_filter($form_state['values']['parent_fields']),
      );
      $form_state['values']['conditions']['references_hierarchy'] = $variables;
    }
  }
}
